# InfiniteInteger #

This is a single class, `InfiniteInteger`, which can be used for performing arbitrary precision integer arithmetic.