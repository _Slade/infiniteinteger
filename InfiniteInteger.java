public class InfiniteInteger implements Comparable<InfiniteInteger>
{
    private final int sign; /* -1 == negative, 0 == 0, 1 == positive */
    private final int[] digits;

    /**
     * Constructor: Constructs this infinite integer from a string
     * representing an integer.
     * @param s  a string representing an integer
     */
    public InfiniteInteger(String s)
    {
        s = s.trim();
        if (s.matches("^-?0*"))
        {
            digits = new int[] { 0 };
            sign   = 0;
            return;
        }

        sign = s.charAt(0) == '-' ? -1 :  1;

        /* Strip non-digits    */
        s = s.replaceAll("[^0-9]", "");
        /* Strip leading zeros */
        s = s.replaceAll("^0*", "");

        digits = new int[ s.length() ];
        for (int i = 0; i < s.length(); ++i)
        {
            digits[i] = s.charAt(i) - 0x30;
        }
    }

    /**
     * Constructor: Constructs this infinite integer from an integer.
     * @param anInteger  an integer
     */
    public InfiniteInteger(int anInteger)
    {
        if (anInteger == 0) // new InfiniteInteger(0) should be fast
        {
            digits = new int[] { 0 };
            sign = 0;
            return;
        }
        // Since Java doesn't allow booleans to be used as integers
        sign = (anInteger > 0 ? 1 : 0) - (anInteger < 0 ? 1 : 0);
        anInteger = anInteger > 0 ? anInteger : -anInteger;

        final int LENGTH =
            anInteger != 0
            ? (int) (Math.log10(anInteger) + 1)
            : 1
        ;

        digits = new int[LENGTH];
        for (int i = LENGTH - 1; i >= 0; --i)
        {
            digits[i] = anInteger % 10;
            anInteger /= 10;
        }
    }

    /* Construct an InfiniteInteger from its constituent attributes */
    private InfiniteInteger(int[] givenDigits, int givenSign)
    {
        sign   = givenSign;
        digits = removeLeadingZeros(givenDigits);
    }

    /**
     * Gets the number of digits of this infinite integer.
     * @return an integer representing the number of digits
     * of this infinite integer.
     */
    public int getNumberOfDigits()
    {
        return digits.length;
    }

    /**
     * Checks whether this infinite integer is a negative number.
     * @return true if this infinite integer is a negative number.
     * Otherwise, return false.
     */
    public boolean isNegative()
    {
        return sign < 0;
    }

    /**
     * Calculates the result of this infinite integer plus other
     * @param other the infinite integer to be added to this
     * infinite integer.
     * @return a NEW infinite integer representing the result of this
     * infinite integer plus other
     */
    public InfiniteInteger plus(final InfiniteInteger other)
    {
        /* I'm unaware of a more elegant way of handling this other than
           enumerating all cases. */

        // IDENTITY OPERATIONS
        // a + 0 = a
        if (sign == 0)
            return other.clone();
        // b + 0 = b
        if (other.sign == 0)
            return this.clone();

        // Take these references to make what follows more readable
        final int[] a = digits;
        final int[] b = other.digits;

        // a + b = a + b
        if (sign > 0 && other.sign > 0)
            return new InfiniteInteger( add(a, b), 1 );
        /*
            This one is more complex. Since our subtract() method requires the larger
            number to be passed as the first parameter, in order to avoid redundant
            comparisons or needing to pass the result of a comparison as an argument
            to subtract(), we handle the four cases as follows:
                                        |a| > |b|  |  |b| < |a|
                                      +------------+------------+
            sgn(a) =  1, sgn(b) = -1  |    a - b   |  -(b - a)  |
            sgn(a) = -1, sgn(b) =  1  |  -(a - b)  |    b - a   |
                                      +-------------------------+
                                          ^            ^
            Signum is +1 or -1 ___________|____________|
        */
        if (sign != other.sign)
        {
            final int CMP_ABS = this.compareAbs(other);
            // a = b -> a - b = 0 and b - a = 0
            if (CMP_ABS == 0)
                return new InfiniteInteger(0);

            if (CMP_ABS > 0 && sign > 0)
                return new InfiniteInteger( subtract(a, b),  1 );

            if (CMP_ABS > 0 && sign < 0)
                return new InfiniteInteger( subtract(a, b), -1 );

            if (CMP_ABS < 0 && sign > 0)
                return new InfiniteInteger( subtract(b, a), -1 );

            if (CMP_ABS < 0 && sign < 0)
                return new InfiniteInteger( subtract(b, a),  1 );
        }
        // (-a) + (-b) = -(a + b)
        if (sign < 0 && other.sign < 0)
            return new InfiniteInteger( add(a, b), -1 );

        throw new RuntimeException(
            "This should NEVER be reached (and probably means an InfiniteInteger's"
            + " private fields have been tampered with in a way they shouldn't be)"
        );
    }

    private static int[] add(int[] a, int[] b)
    {
        int carryOver  = 0;
        final int BASE = 10;
        int[] larger;
        int[] smaller;
        if (a.length == b.length)
        {
            larger  = a;
            smaller = b;
        }
        else
        {
            larger  = a.length > b.length ? a : b;
            smaller = a.length > b.length ? b : a;
        }
        final int L_LEN = larger.length;
        int[] newNum = new int[ L_LEN + 1 ];

        if (L_LEN != smaller.length)
        {
            smaller = addLeadingZeros(smaller, L_LEN);
        }

        for (int i = L_LEN; i > 0; --i)
        {
            final int sum =
                  larger[i - 1]
                + smaller[i - 1]
                + carryOver
            ;
            newNum[i] = sum % BASE;
            carryOver = sum / BASE;
        }
        newNum[0] = carryOver;
        return removeLeadingZeros(newNum);
    }

    /* Using the 9's complement method for subtraction. The goal is to keep
    logic related to special cases like zero and different signs inside of
    the plus/minus methods, while add/subtract ONLY handle the addition and
    subtraction algorithms for the digits arrays. */
    private static int[] subtract(int[] minuend, int[] subtrahend)
    {
        if (minuend.length != subtrahend.length)
            subtrahend = addLeadingZeros(subtrahend, minuend.length);
        subtrahend = ninesComplement(subtrahend);
        int[] intermediateSum = add(minuend, subtrahend);
        int[] sum = new int[ intermediateSum.length - 1 ];
        System.arraycopy(intermediateSum, 1, sum, 0, sum.length);
        int[] plusOne = new int[ sum.length ];
        plusOne[ plusOne.length - 1 ] = 1;
        return add(sum, plusOne);
    }

    private static int[] ninesComplement(int[] numArray)
    {
        int[] comp = new int[ numArray.length ];
        for (int i = 0; i < numArray.length; ++i)
        {
            comp[i] = Math.abs(9 - numArray[i]);
        }
        return comp;
    }

    /**
     * Calculates the result of this infinite integer subtracted by other
     * @param other the infinite integer to subtract.
     * @return a NEW infinite integer representing the result of this
     * infinite integer subtracted by other
     */
    public InfiniteInteger minus(final InfiniteInteger other)
    {
        return this.plus(other.oppositeShallow());
    }

    /**
     * Calculates the result of this infinite integer multiplied by other
     * @param other the multiplier.
     * @return a NEW infinite integer representing the result of this
     * infinite integer multiplied by other.
     */
    public InfiniteInteger multiply(final InfiniteInteger other)
    {
        final int NEWSIGN = sign * other.sign;
        if (NEWSIGN == 0)
            return new InfiniteInteger(0);
        final int CMP = this.compareAbs(other);
        final int[] NEWDIGITS =
            CMP >= 0
            ? multiplyHelper(digits, other.digits)
            : multiplyHelper(other.digits, digits)
        ;
        return new InfiniteInteger(NEWDIGITS, NEWSIGN);
    }

    private int[] multiplyHelper (int[] larger, int[] smaller)
    {
        /* floor(log_10(m) + log_10(n)) = floor(log_10(m*n))
         * In other words, we can be certain that this is a large
         * enough allocation to fit the product of m and n.
         */
        int[] newDigits = new int[ larger.length + smaller.length ];
        int[] step      = new int[ larger.length + smaller.length ];
        for (int eachS = smaller.length - 1; eachS >= 0; --eachS)
        {
            int carryOver = 0;
            for (int eachL = larger.length - 1; eachL >= 0; --eachL)
            {
                final int PRODUCT = smaller[eachS] * larger[eachL] + carryOver;
                step[ eachL + eachS + 1 ] = PRODUCT % 10;
                carryOver = PRODUCT / 10;
            }
            step[eachS] = carryOver;
            newDigits = add(newDigits, step);
            // Reuse step's memory for efficiency's sake.
            for (int i = 0; i < step.length; ++i)
            {
                step[i] = 0;
            }
        }
        return newDigits;
    }

    private static int[] addLeadingZeros(int[] array, int newLength)
    {
        final int ARR_LEN = array.length;
        if (newLength <= ARR_LEN)
        {
            System.err.println(
                "Bad length given to addLeadingZeros: "
                + java.util.Arrays.toString(array)
                + " " + newLength
            );
        }
        int[] padded = new int[ newLength ];
        System.arraycopy(array, 0, padded, newLength - ARR_LEN, ARR_LEN);
        return padded;
    }

    private static int[] removeLeadingZeros(int[] array)
    {
        if (array[0] != 0 || array.length == 1)
            return array;

        int start = 0;
        for (int i = 0; i < array.length && array[i] == 0; ++i)
            start = i + 1;

        int[] trimmed = new int[ array.length - start ];
        System.arraycopy(array, start, trimmed, 0, array.length - start);
        return trimmed;
    }

    /**
     * Returns the opposite of this InfiniteInteger (that is, this multiplied by -1).
     * @return A new InfiniteInteger whose value is the opposite of the invocant's.
     */
    public InfiniteInteger opposite()
    {
        return new InfiniteInteger(digits.clone(), -1 * sign);
    }

    private InfiniteInteger oppositeShallow()
    {
        return new InfiniteInteger(digits, -1 * sign);
    }

    /**
     * Generates a string representing this infinite integer. If this infinite
     * integer is a negative number a minus symbol should be in the front of
     * numbers. For example, "-12345678901234567890". But if the infinite
     * integer is a positive number, no symbol should be in the front of the
     * numbers (e.g., "12345678901234567890"). @return a string representing
     * this infinite integer number.
     */
    public String toString()
    {
        final int DIGITS = this.getNumberOfDigits();
        char[] digitsAsChars;
        if (this.isNegative())
        {
            digitsAsChars = new char[1 + DIGITS];
            digitsAsChars[0] = 0x2D; // ASCII hyphen
            for (int i = 1; i < DIGITS + 1; ++i)
                digitsAsChars[i] = (char) (digits[i - 1] + 0x30);
        }
        else
        {
            digitsAsChars = new char[DIGITS];
            for (int i = 0; i < DIGITS; ++i)
                digitsAsChars[i] = (char) (digits[i] + 0x30);
        }
        return String.valueOf(digitsAsChars);
    }

    public boolean equals(final InfiniteInteger other)
    {
        return this.compareTo(other) == 0;
    }

    /**
     * Compares this InfiniteInteger with another InfiniteInteger.
     * @param other The InfiniteInteger to compare the invocant to.
     * @return either -1, 0, or 1 as follows:
     * If this infinite integer is less than other, return -1.
     * If this infinite integer is equal to other, return 0.
     * If this infinite integer is greater than other, return 1.
     */
    public int compareTo(final InfiniteInteger other)
    {
        if (sign == other.sign)
        {
            switch (sign)
            {
                default:
                    return 0;
                case -1:
                    return other.compareAbs(this);
                case 1:
                    return this.compareAbs(other);
            }
        }
        return sign > other.sign ? 1 : -1;
    }

    /**
     * Compares this InfiniteInteger with another InfiniteInteger, ignoring signs.
     * @param other The InfiniteInteger to compare the invocant to.
     * @return either -1, 0, or 1 as follows:
     * If this infinite integer's absolute value is less than other, return -1.
     * If this infinite integer's absolute value is equal to other, return 0.
     * If this infinite integer's absolute value is greater than other, return 1.
     */
    public int compareAbs(final InfiniteInteger other)
    {
        if (digits.length > other.digits.length)
            return  1;
        if (digits.length < other.digits.length)
            return -1;
        // this.digits.length == other.digits.length
        for (int i = 0; i < digits.length; ++i)
        {
            if (digits[i] > other.digits[i])
                return  1;
            if (digits[i] < other.digits[i])
                return -1;
        }
        return 0;
    }

    @Override // Object.clone() is protected and the Cloneable interface is broken
    public InfiniteInteger clone()
    {
        return new InfiniteInteger(digits.clone(), sign);
    }

    public boolean deeplyEquals(final InfiniteInteger other)
    {
        if (digits.length != other.digits.length)
            return false;
        if (isNegative() != other.isNegative())
            return false;
        for (int i = 0; i < digits.length; ++i)
            if (digits[i] != other.digits[i])
                return false;
        return true;
    }
}
